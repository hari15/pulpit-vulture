# This file is used by Rack-based servers to start the application.

# Unicorn Suicide bomber - Kills worker which matches either of below criteria.
require 'unicorn/worker_killer'

# Max requests per worker - Suicide when you have handled any random number of requests
# between 3072 to 4096. We arrived at these numbers when we used to get 370 requests/min
# with 8 worker threads.
use Unicorn::WorkerKiller::MaxRequests, 3072, 4096

# Max memory size (RSS) per worker
# If the worker is taking anywhere between 800 MB to 850 MB, suicide.
# Anything above that on a 15 GB mem machine is going to take the site down.
use Unicorn::WorkerKiller::Oom, (800*(1024**2)), (850*(1024**2))

require ::File.expand_path('../config/environment', __FILE__)
run Rails.application
