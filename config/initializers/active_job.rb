beanstalk_cfg = Rails.application.secrets.beanstalk
if beanstalk_cfg
  Rails.application.configure do
    config.active_job.queue_adapter = :backburner
    config.active_job.queue_name_delimiter = '.'
  end

  require 'backburner'

  Backburner.configure do |config|
    config.beanstalk_url    = ["beanstalk://%s:%s" % [beanstalk_cfg['host'], beanstalk_cfg['port']]]
    config.tube_namespace   = [Rails.application.secrets.namespace, Rails.env].join('.')
    config.on_error         = lambda { |e| puts e }
    config.max_job_retries  = 3 # default 0 retries
    config.retry_delay      = 2 # default 5 seconds
    config.default_priority = 65536
    config.respond_timeout  = 120
    config.default_worker   = Backburner::Workers::Simple
    config.logger           = Logger.new('log/backburner.log')
#    config.retry_delay_proc    = lambda { |min_retry_delay, num_retries| min_retry_delay + (num_retries ** 3) }
#    config.primary_queue       = "backburner-jobs"
    config.priority_labels  = { :custom => 50, :useless => 1000 }
    config.reserve_timeout  = nil
  end
end
