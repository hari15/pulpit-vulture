if ['production', 'staging'].include?(Rails.env)
  require 'airbrake'

  Airbrake.configure do |config|
    # Uncomment this for local testing
    # config.development_environments = []

    config.api_key = Rails.application.secrets.airbrake['key']

    config.async do |notice|
      AirbrakeDeliveryJob.perform_later(notice.to_xml)
    end
  end
end
