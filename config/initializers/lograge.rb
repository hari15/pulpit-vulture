require 'lograge'
require 'pulpit/logging/formatters'

Rails.application.configure do
  config.lograge.enabled = true
  config.lograge.logger = ActiveSupport::Logger.new "#{Rails.root}/log/#{Rails.env}.log"
  # ASSUMPTION: Rails logger has already been initialized in the respective environment file.
  config.lograge.logger.level = Rails.logger.level || :debug
  config.lograge.logger.formatter = Pulpit::Logging::Formatters::Json.new
  config.lograge.formatter = Lograge::Formatters::Raw.new
  config.lograge.custom_options = ->(event) {
    {
      time: event.time,
      params: event.payload[:params].except('controller', 'action', 'format', 'utf8')
    }
  }
  Rails.logger = config.lograge.logger
end
