module Pulpit
  module API
    module V1

      require './lib/clients/blueshift.rb'
      require './lib/clients/intercomm.rb'
      require './lib/clients/urban_ladder.rb'

      class Analytics < Grape::API
        include Pulpit::API::Base

        version 'v1', using: :path

        before do
          def add_request_meta_info
            params[:data]['user_agent'] = request.env['HTTP_USER_AGENT']
            params[:data]['ip'] = request.try(:remote_ip) || request.env['HTTP_X_REAL_IP']
            params[:data]['timestamp_epoch'] = timestamp_epoch
            params[:data]['device'] = device
            params[:data]['platform'] = platform
            params[:data]['originalTimestamp'] = original_timestamp
            params[:data]['messageId'] = "#{params[:data]['timestamp_epoch']}-" +
                                         "#{params[:data]['anonymousId']}-" +
                                         (('A'..'Z').to_a + (0..9).to_a).sample(10).join
          end

          def timestamp_epoch
            begin
              if params[:data]['originalTimestamp'].to_s.match(/\A\d+\z/i) &&
                 params[:data]['originalTimestamp'].to_s.length >= 10
                params[:data]['originalTimestamp'].to_s[0..9].to_i
              else
                DateTime.parse(params[:data]['originalTimestamp'].to_s).strftime('%s').to_i
              end
            rescue
              Time.now.strftime('%s').to_i
            end
          end

          def platform
            user_agent = request.env['HTTP_USER_AGENT']
            return 'web' unless user_agent.present?

            if user_agent.include?('Urban Ladder') && user_agent.include?('Android')
              'android_app'
            elsif user_agent.include?('com.urbanladder.Catalog-App')
              if user_agent.include?('iPhone')
                'iphone_app'
              elsif user_agent.include?('iPad')
                'ipad_app'
              else
                'ios_app'
              end
            elsif user_agent.include?('Mozilla') || user_agent.include?('Chrome') || user_agent.include?('Safari')
              if user_agent.include?('Android') && user_agent.include?('Linux') && user_agent.include?('Mobile')
                'android_web'
              elsif user_agent.include?('Mac OS X') && user_agent.include?('Mobile')
                if user_agent.include?('iPhone')
                  'iphone_web'
                elsif user_agent.include?('iPad')
                  'ipad_web'
                else
                  'ios_web'
                end
              elsif user_agent.include?('Mobile')
                'mobile_web'
              else
                'web'
              end
            else
              'web'
            end
          end

          def device
            request_platform = platform
            if request_platform.include?('app')
              'app'
            elsif request_platform == 'web'
              'web'
            else
              'mweb'
            end
          end

          def original_timestamp
            Time.at(params[:data]['timestamp_epoch'])
          end
        end

        resource :analytics do
          desc 'Receives analytics data and sends to respective clients'
          params do
            requires :event_type, type: String, allow_blank: false, desc: 'type of event eg: page_view, add_to_cart etc'
            requires :data, type: Hash, allow_blank: false, desc: 'Event data'
            optional :send_to, type: String, allow_blank: false, default: 'all'
          end
          post do
            add_request_meta_info

            if params[:send_to] == 'all'
              Parallel.each(GlobalConstants::CLIENTS, in_threads: 2) do |client|
                "#{client.camelize}::Client".constantize.new.send_events(params[:event_type], params[:data])
              end
            end
            success_response({ data: {}.to_json }, 'analytics', status: 201)
          end
        end
      end
    end
  end
end
