require 'grape'

module Pulpit
  module API
    class Analytics < Grape::API
      mount Pulpit::API::V1::Analytics

      module HealthCheckHelpers
        extend Grape::API::Helpers

        def elb
          true
        end

        def monit
          {}
        end
      end
      namespace :analytics do
        # NOTE: HealthCheckHelpers module has to be defined before this inclusion
        include Pulpit::API::HealthChecks
      end

      # NOTE: This block *MUST* be the last entry in this file because it
      # handles all 404s.
      include Pulpit::API::NotFound
    end
  end
end
