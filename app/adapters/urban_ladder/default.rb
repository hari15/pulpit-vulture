module Pulpit
  module Adapters
    module UrbanLadder
      class Default
        def adapt(type, event_data)
          if event_data['event'] == 'Completed Order'
            event_data['properties']['products'] = event_data['properties']['products'].values
          end

          {
            type: type,
            anonymousId: event_data['anonymousId'],
            context: {
              page: {
                path: event_data['path'],
                referrer: '',
                search: event_data['search'],
                title: event_data['title'],
                url: event_data['url']
              },
              userAgent: event_data['user_agent'],
              ip: event_data['ip']
            },
            event: event_data['event'],
            messageId: event_data['messageId'],
            properties: event_data['properties'],
            originalTimestamp: event_data['originalTimestamp'],
            channel: event_data['channel']
          }
        end
      end
    end
  end
end
