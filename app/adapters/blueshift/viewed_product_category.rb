module Pulpit
  module Adapters
    module Blueshift
      class ViewedProductCategory
        def adapt(_type, event_data)
          page_info = event_data['context']['page']

          {
            timestamp: event_data['originalTimestamp'],
            event: 'viewed product category',
            referrer: page_info['referrer'],
            ip: event_data['ip'],
            cookie: event_data['anonymousId'],
            url: page_info['url'],
            category: event_data['properties']['category'],
            currency: 'INR',
            anonymousid: event_data['anonymousId'],
            user_agent: event_data['user_agent'],
            timestamp_epoch: event_data['timestamp_epoch'],
            messageId: event_data['messageId']
          }
        end
      end
    end
  end
end
