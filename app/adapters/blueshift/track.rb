module Pulpit
  module Adapters
    module Blueshift
      class Track
        def adapt(type, event_data)
          event = event_data["event"]
          sub_type =
            if event == 'Added Product' || event == 'add_to_cart'
              'add_to_cart'
            elsif event.present?
              event.downcase.gsub(' ', '_')
            else
              'default'
            end

          new_adapter =
            begin
              "Pulpit::Adapters::Blueshift::#{sub_type.camelize}".constantize.new
            rescue
              'Pulpit::Adapters::Blueshift::Default'.constantize.new
            end

          new_adapter.adapt(type, event_data)
        end
      end
    end
  end
end
