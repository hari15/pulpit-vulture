module Pulpit
  module Adapters
    module Blueshift
      class AddToCart
        def adapt(type, event_data)
          variant_info = event_data['properties']
          page_info = event_data['context']['page']

          {
            timestamp: event_data['originalTimestamp'],
            event: 'add_to_cart',
            referrer: page_info['referrer'],
            ip: event_data['ip'],
            cookie: event_data['anonymousId'],
            url: page_info['url'],
            id: variant_info['sku'],
            pid: variant_info['product_id'], # check
            currentvariantname: variant_info['name'], # check
            sku: variant_info['sku'],
            price: variant_info['price'],
            currency: 'INR',
            category: variant_info['category'],
            availability: variant_info['availability'],
            herosku: variant_info['herosku'], # check
            anonymousid: event_data['anonymousId'],
            user_agent: event_data['user_agent'],
            product_ids: [variant_info['sku']],
            timestamp_epoch: event_data['timestamp_epoch'],
            messageId: event_data['messageId'],

            discounted_price: variant_info['discounted_price'],
            lowest_emi: variant_info['lowest_emi']
          }
        end
      end
    end
  end
end
