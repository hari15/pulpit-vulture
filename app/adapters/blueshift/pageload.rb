module Pulpit
  module Adapters
    module Blueshift
      class Pageload
        def adapt(type, event_data)
          {
            timestamp: event_data['originalTimestamp'],
            event: 'pageload',
            referrer: event_data['referrer'],
            ip: event_data['ip'],
            cookie: event_data['anonymousId'],
            url: event_data['url'],
            name: event_data['page_type'],
            path: event_data['path'],
            title: event_data['title'],
            anonymousid: event_data['anonymousId'],
            user_agent: event_data['user_agent'],
            timestamp_epoch: event_data['timestamp_epoch'],
            messageId: event_data['messageId']
          }
        end
      end
    end
  end
end
