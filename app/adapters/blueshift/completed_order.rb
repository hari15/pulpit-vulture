module Pulpit
  module Adapters
    module Blueshift
      class CompletedOrder
        def adapt(_type, event_data)
          order_info = event_data['properties']
          page_info = event_data['context']['page']

          products = order_info['products'].values.map do |product|
            {
              'name' => product['name'],
              'quantity' => product['quantity'].to_i,
              'price' => product['price'],
              'category' => product['category'],
              'sku' => product['sku']
            }
          end

          # product_ids = products.map { |product| product['sku'] }

          {
            timestamp: event_data['originalTimestamp'],
            event: 'purchase',
            referrer: page_info['referrer'],
            ip: event_data['ip'],
            cookie: event_data['anonymousId'],
            url: page_info['url'],
            order_id: order_info['order_id'],
            email: order_info['email'],
            revenue: order_info['revenue'],
            discount: order_info['discount'],
            paid: order_info['paid'],
            to_be_paid: order_info['to_be_paid'],
            mode_of_payment: order_info['mode_of_payment'],
            currency: 'INR',
            anonymousid: event_data['anonymousId'],
            user_agent: event_data['user_agent'],
            products: products,
            # TODO figure out why uncommenting the below line removes "product_ids" from blob in blueshift
            # product_ids: product_ids,
            timestamp_epoch: event_data['timestamp_epoch'],
            messageId: event_data['messageId']
          }
        end
      end
    end
  end
end
