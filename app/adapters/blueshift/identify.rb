module Pulpit
  module Adapters
    module Blueshift
      class Identify
        def adapt(type, event_data)
          firstname = event_data['firstname'] || event_data['name'].to_s.split(' ').first
          lastname = event_data['firstname'] || event_data['name'].to_s.split(' ').last
          {
            event: 'identify',
            type: 'identify',
            unsubscribed: false,
            id: event_data['userId'],
            customer_id: event_data['userId'],
            cookie: event_data['anonymousId'],
            anonymousid: event_data['anonymousId'],
            email: event_data['email'],
            name: event_data['name'],
            firstname: firstname,
            lastname: lastname,
            url: event_data['url'],
            referrer: event_data['referrer'],
            device_type: event_data['device'],
            user_agent: event_data['user_agent'],
            platform: event_data['platform'],
            ip: event_data['ip'],
            timestamp_epoch: event_data['timestamp_epoch'],
            messageId: event_data['messageId']
          }
        end
      end
    end
  end
end
