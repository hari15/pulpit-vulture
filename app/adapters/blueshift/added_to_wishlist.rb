module Pulpit
  module Adapters
    module Blueshift
      class AddedToWishlist
        def adapt(_type, event_data)
          variant_info = event_data['properties']
          page_info = event_data['context']['page']

          {
            timestamp: event_data['originalTimestamp'],
            event: 'added to wishlist',
            referrer: page_info['referrer'],
            ip: event_data['ip'],
            cookie: event_data['anonymousId'],
            url: page_info['url'],
            sku: variant_info['SKU'],
            name: variant_info['Name'], # check
            price: variant_info['Price'],
            variant: variant_info['variant'],
            currency: 'INR',
            anonymousid: event_data['anonymousId'],
            user_agent: event_data['user_agent'],
            product_ids: [variant_info['SKU']],
            timestamp_epoch: event_data['timestamp_epoch'],
            messageId: event_data['messageId'],

            discounted_price: variant_info['discounted_price'],
            lowest_emi: variant_info['lowest_emi']
          }
        end
      end
    end
  end
end
