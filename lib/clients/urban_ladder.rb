require './app/adapters/urban_ladder/default.rb'

module UrbanLadder
  class Client
    require 'kafka'

    attr_reader :kafka

    def initialize
      @kafka = Kafka.new(
        seed_brokers: ['localhost:9092'],
        client_id: 'my-application',
      )
    end

    def send_events(type, event_data)
      adapter = 'Pulpit::Adapters::UrbanLadder::Default'.constantize.new

      formatted_data = adapter.adapt(type, event_data)

      kafka.deliver_message(formatted_data.to_json, topic: "test")
    end
  end
end
