require './app/adapters/blueshift/identify.rb'
require './app/adapters/blueshift/pageload.rb'
require './app/adapters/blueshift/track.rb'
require './app/adapters/blueshift/add_to_cart.rb'
require './app/adapters/blueshift/viewed_product.rb'
require './app/adapters/blueshift/added_to_wishlist.rb'
require './app/adapters/blueshift/viewed_product_category.rb'
require './app/adapters/blueshift/completed_order.rb'
require './app/adapters/blueshift/default.rb'

module Blueshift
  class Client
    include HTTParty

    STATUS_OK = '200'

    attr_reader :auth, :api_endpoint

    def initialize
      blueshift_config = CONFIG['blueshift']
      self.class.base_uri blueshift_config['base_url']
      @auth = { username: blueshift_config['api_key'] }
      @api_endpoint = '/api/v1/event'
    end

    def send_events(type, event_data)
      adapter =
        begin
          "Pulpit::Adapters::Blueshift::#{type.camelize}".constantize.new
        rescue
          'Pulpit::Adapters::Blueshift::Default'.constantize.new
        end
      formatted_data = adapter.adapt(type, event_data)

      return if formatted_data.blank?

      status_obj = self.class.post(api_endpoint,  { body: formatted_data.to_json,
                                                    basic_auth: auth,
                                                    headers: { 'Content-Type' => 'application/json',
                                                               'Accept' => 'application/json'}})
      if status_obj.response.code != STATUS_OK
        raise "Event syncing to Blueshift failed with code : #{status_obj.response.code}, event: #{formatted_data}"
      end
    end
  end
end
