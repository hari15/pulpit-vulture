def call_hook(hookname)
  pulpit_path = `bundle show pulpit`.strip
  require_relative "#{pulpit_path}/deploy/#{hookname}.rb"
  send(hookname)
end
